# Extract all files from network traffic

from scapy.all import *
import sys
import os

# transaction id(2) + protocol identifier(2) + length(2) + unit identifier(1) (Not include function code byte)
MODBUS_HDR_LEN = 7

if len(sys.argv) < 2:
    print "Usage: python extractor_down.py pcapfile"
    sys.exit(0)

all_pkts = rdpcap(sys.argv[1])

file_path = str(os.path.abspath(sys.argv[1]))
save_dir = file_path.split(".")[0] + "/download/"

pkts = (pkt for pkt in all_pkts if
    TCP in pkt and (pkt[TCP].dport == 502) and len(pkt[TCP].payload) > MODBUS_HDR_LEN)

next_addr = 0    # initialization

for pkt in pkts:
    # M221 protocol message which is used between SoMachine Basics and M221 PLC
    msg = bytes(pkt[TCP].payload)[MODBUS_HDR_LEN+1:]

    # write request
    if msg[1] == '\x29':
        addr = msg[2:4]
        fileType = msg[4:6]
        size = msg[6:8]

        if next_addr != struct.unpack("<H", addr)[0]:
            save_file_path = save_dir + binascii.hexlify(msg[2:6])
            
            if not os.path.exists(os.path.dirname(save_file_path)):
                try:
                    os.makedirs(os.path.dirname(save_file_path))
                except:
                    print "exception occur in making dir: " + os.path.dirname(os.path.dirname(save_file_path))

            print "Create file: " + save_file_path
            out_file = open(save_file_path, "w")
#        else:
#            if "prev_fileType" in globals():
#                if prev_fileType != fileType:
#                    print "Continuous block, but file type is different"        
           
        out_file.write(msg[8:])
        next_addr = struct.unpack("<H", addr)[0] + struct.unpack("<H", size)[0]
        prev_fileType = fileType

