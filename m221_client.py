import sys, os
import struct
import binascii
import socket
import argparse
import time

# M221 message (modbus payload after the function code) offset in TCP payload
M221_OFFSET = 8 
MODBUS_PORT = 502

class M221_client():
    def __init__(self, targetIP):
        self.tranID = 1
        self.proto = '\x00\x00'
        self.len = 0
        self.unitID = '\x01'
        # Function code: Unity (Schneider) (90)
        self.fnc = '\x5a'

        self.m221_sid = '\x00'

        self.send_counter = 0

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((targetIP, MODBUS_PORT))

        self.set_m221_session_id()

    def send_recv_msg(self, modbus_data):
        self.send_counter += 1
        print "#", self.send_counter, "#"

        self.len = len(modbus_data) + len(self.unitID) + len(self.fnc)  
        tcp_payload = struct.pack(">H", self.tranID) + self.proto + struct.pack(">H", self.len) + self.unitID + self.fnc + modbus_data
        self.tranID = (self.tranID + 1) % 65536

        self.sock.send(tcp_payload)
        
        s = binascii.hexlify(tcp_payload)
        t = iter(s)
        print "--> " + ':'.join(a+b for a,b in zip(t,t)) + " (" + str(len(tcp_payload)) + ")"

        recv_buf = self.sock.recv(1000)
        r = binascii.hexlify(recv_buf)
        t = iter(r)
        print "<-- " + ':'.join(a+b for a,b in zip(t,t)) + " (" + str(len(recv_buf)) + ")"

        return recv_buf

    def close_socket(self):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

    def close_connection(self):
        modbus_data = self.m221_sid + '\x11'
        self.send_recv_msg(modbus_data)
        self.close_socket()

    def set_m221_session_id(self):
        sid_req_payload = '\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
        self.m221_sid = self.send_recv_msg(sid_req_payload)[-1]
        print "m221 session id: " + binascii.hexlify(self.m221_sid)

    """
    def write_program(self):
        modbus_data = self.m221_sid + '\x29' + '\xb0\xe0\x01\x07\x06\x00\x7c\x2c\xfd\xe0\x2d\x02'

        self.send_recv_msg(modbus_data)
    """

    # Send read requests to PLC to get a file
    def read_file(self, file_addr, file_type, file_size):
        max_data_unit = 236
        remained = file_size
        file_buf = ''
        
        while (remained > 0):
            if remained >= max_data_unit:
                fragment_size = max_data_unit
            else:
                fragment_size = remained
            # read request: 0x28
            modbus_data = '\x00\x28' + struct.pack("<H", file_addr) + file_type + struct.pack("<H", fragment_size)
            file_buf += self.send_recv_msg(modbus_data)[M221_OFFSET+4:] # 0x00fe + response data size (2 bytes)
            remained -= fragment_size
            file_addr += fragment_size
        return file_buf        

    def upload(self, upload_directory):
        conf_file_name = self.read_conf_file(upload_directory)
        conf2_file_name = self.read_conf2_file(upload_directory, conf_file_name)

        self.read_zip_hash_file(upload_directory, conf_file_name)

        self.read_data_file(upload_directory, conf2_file_name)
        self.read_data2_file(upload_directory, conf2_file_name)

        self.read_logic_file(upload_directory, conf2_file_name)

        self.read_ff4e_file(upload_directory)
        self.read_fffb_file(upload_directory)
        
    # conf file: d4fe0100
    def read_conf_file(self, upload_directory):
        conf_file_name, conf_file_addr, conf_file_type, conf_file_size = self.get_conf_file_info()
        conf_file_data = self.read_file(conf_file_addr, conf_file_type, conf_file_size)
        save_file(upload_directory, conf_file_name, conf_file_data)
        return conf_file_name

    # contains addresses of control logic binary and data
    def read_conf2_file(self, upload_directory, conf_file_name):
        conf2_file_name, conf2_file_addr, conf2_file_type, conf2_file_size = self.get_conf2_file_info(upload_directory, conf_file_name)
        conf2_file_data = self.read_file(conf2_file_addr, conf2_file_type, conf2_file_size)
        save_file(upload_directory, conf2_file_name, conf2_file_data)
        return conf2_file_name

    # file containing a zip file and a password hash if exists
    def read_zip_hash_file(self, upload_directory, conf_file_name):
        zip_file_name, zip_file_addr, zip_file_type, zip_file_size = self.get_zip_hash_file_info(upload_directory, conf_file_name)
        zip_file_data = self.read_file(zip_file_addr, zip_file_type, zip_file_size)
        save_file(upload_directory, zip_file_name, zip_file_data)
        
    # file containing data such as timer, etc.
    def read_data_file(self, upload_directory, conf2_file_name):
        data_file_name, data_file_addr, data_file_type, data_file_size = self.get_data_file_info(upload_directory, conf2_file_name)
        data_file_data = self.read_file(data_file_addr, data_file_type, data_file_size)
        save_file(upload_directory, data_file_name, data_file_data)
        
    # control logif file (RX630 binary)
    def read_logic_file(self, upload_directory, conf2_file_name):
        logic_file_name, logic_file_addr, logic_file_type, logic_file_size = self.get_logic_file_info(upload_directory, conf2_file_name)
        logic_file_data = self.read_file(logic_file_addr, logic_file_type, logic_file_size)
        save_file(upload_directory, logic_file_name, logic_file_data)

    # description of data file (08000107)
    def read_data2_file(self, upload_directory, conf2_file_name):
        data2_file_name, data2_file_addr, data2_file_type, data2_file_size = self.get_data2_file_info(upload_directory, conf2_file_name)
        data2_file_data = self.read_file(data2_file_addr, data2_file_type, data2_file_size)
        save_file(upload_directory, data2_file_name, data2_file_data)

    # unknown file(?) it has only 1 byte size
    def read_ff4e_file(self, upload_directory):
        ff4e_file_addr, ff4e_file_type, ff4e_file_size = 0xff4e, '\x01\x00', 1
        ff4e_file_name = binascii.hexlify(struct.pack("<H", ff4e_file_addr) + ff4e_file_type)
        ff4e_file_data = self.read_file(ff4e_file_addr, ff4e_file_type, ff4e_file_size)
        save_file(upload_directory, ff4e_file_name, ff4e_file_data)

    # unknown file(?) it has only 1 byte size
    def read_fffb_file(self, upload_directory):
        fffb_file_addr, fffb_file_type, fffb_file_size = 0xfffb, '\x01\x00', 1
        fffb_file_name = binascii.hexlify(struct.pack("<H", fffb_file_addr) + fffb_file_type)
        fffb_file_data = self.read_file(fffb_file_addr, fffb_file_type, fffb_file_size)
        save_file(upload_directory, fffb_file_name, fffb_file_data)

    def get_conf_file_info(self):
        conf_file_addr = 0xfed4             # always same
        conf_file_type = '\x01\x00'         # always same
        conf_file_size = 300                # always same
        conf_file_name = binascii.hexlify(struct.pack("<H", conf_file_addr) + conf_file_type)

        return conf_file_name, conf_file_addr, conf_file_type, conf_file_size

    def get_conf2_file_info(self, directory, conf_file_name):
        conf_data = read_local_file(directory, conf_file_name)
        conf2_file_addr = struct.unpack("<H", conf_data[0x68:0x6a])[0]
        conf2_file_type = conf_data[0x6a:0x6c]                           # always '\x04\x07'?
        conf2_file_size = struct.unpack("<H", conf_data[0x6c:0x6e])[0]
        conf2_file_name = binascii.hexlify(struct.pack("<H", conf2_file_addr) + conf2_file_type)

        return conf2_file_name, conf2_file_addr, conf2_file_type, conf2_file_size

    def get_zip_hash_file_info(self, directory, conf_file_name):
        conf_data = read_local_file(directory, conf_file_name)
        zip_file_addr = struct.unpack("<H", conf_data[0x70:0x72])[0]    # always 0xd000?
        zip_file_type = conf_data[0x72:0x74]                            # always 0x0007?
        zip_file_size = struct.unpack("<H", conf_data[0x74:0x76])[0]
        zip_file_name = binascii.hexlify(struct.pack("<H", zip_file_addr) + zip_file_type)

        return zip_file_name, zip_file_addr, zip_file_type, zip_file_size

    def get_data_file_info(self, directory, conf2_file_name):
        conf2_data = read_local_file(directory, conf2_file_name)
        data_file_addr = 0x8000                                         # always 0x8000 ?
        data_file_type = '\x01\x07'                                     # always '\x01\x07'?
        data_file_end_addr = struct.unpack("<H", conf2_data[-36:-34])[0] + struct.unpack("<H", conf2_data[-32:-30])[0]
        data_file_size = data_file_end_addr - data_file_addr
        data_file_name = binascii.hexlify(struct.pack("<H", data_file_addr) + data_file_type)

        return data_file_name, data_file_addr, data_file_type, data_file_size

    def get_logic_file_info(self, directory, conf2_file_name):
        conf2_data = read_local_file(directory, conf2_file_name)
        logic_file_addr = struct.unpack("<H", conf2_data[-24:-22])[0]             
        logic_file_type = '\x01\x07'                                    # always '\x01\x07'?
        logic_file_size = struct.unpack("<H", conf2_data[-20:-18])[0]   
        logic_file_name = binascii.hexlify(struct.pack("<H", logic_file_addr) + logic_file_type)
        
        return logic_file_name, logic_file_addr, logic_file_type, logic_file_size

    def get_data2_file_info(self, directory, conf2_file_name):
        conf2_data = read_local_file(directory, conf2_file_name)
        data2_file_addr = 0x0200                                        # always 0200               
        data2_file_type = '\x00\x00'                                    # always '\x00\x00'?
        data2_file_end_addr = struct.unpack("<H", conf2_data[0x34:0x36])[0] + struct.unpack("<H", conf2_data[0x38:0x3a])[0]
        data2_file_size = data2_file_end_addr - data2_file_addr  
        data2_file_name = binascii.hexlify(struct.pack("<H", data2_file_addr) + data2_file_type)
        
        return data2_file_name, data2_file_addr, data2_file_type, data2_file_size

    # Send write requests to PLC to write a file
    def write_file(self, file_addr, file_type, file_size, file_data):
        if (file_size != len(file_data)):
            print "Error. file size is not match"
            sys.exit(1)
        max_data_unit = 236
        remained = file_size
        offset = 0
        
        while (remained > 0):
            if remained >= max_data_unit:
                fragment_size = max_data_unit
            else:
                fragment_size = remained
            # write request: 0x29
            modbus_data = self.m221_sid + '\x29' + struct.pack("<H", file_addr) + file_type + struct.pack("<H", fragment_size) + file_data[offset:offset+fragment_size]
            self.send_recv_msg(modbus_data)
            
            remained -= fragment_size
            file_addr += fragment_size
            offset += fragment_size

    def write_file_onebyone(self, file_addr, file_type, file_size, file_data):
        if (file_size != len(file_data)):
            print "Error. file size is not match"
            sys.exit(1)
        max_data_unit = 236
        remained = file_size
        offset = 0
        
        while (remained > 0):
            """
            if remained >= max_data_unit:
                fragment_size = max_data_unit
            else:
                fragment_size = remained
            """
            fragment_size = 1
            # write request: 0x29
            modbus_data = self.m221_sid + '\x29' + struct.pack("<H", file_addr) + file_type + struct.pack("<H", fragment_size) + file_data[offset:offset+fragment_size]
            self.send_recv_msg(modbus_data)
            
            remained -= fragment_size
            file_addr += fragment_size
            offset += fragment_size

    def send_single_write(self, addr, loc_type, size, data):
        modbus_data = self.m221_sid + '\x29' + struct.pack("<H", addr) + loc_type + struct.pack("<H", size) + data
        self.send_recv_msg(modbus_data)

    def simple_download(self, download_directory):
        self.write_data_file(download_directory)
        self.write_logic_file(download_directory)    

    
    def write_logic_file_new_loc(self, download_directory, file_name, new_start_addr):
        logic_file_data = read_local_file(download_directory, file_name)

        #self.write_file(new_start_addr, '\x01\x07', len(logic_file_data), logic_file_data)
        #self.write_file(new_start_addr, '\x04\x07', len(logic_file_data), logic_file_data)
        self.write_file_onebyone(new_start_addr, '\x04\x07', len(logic_file_data), logic_file_data)


    def adjust_code_start(self, new_start_addr):
        #self.send_single_write(0x56b0, '\x01\x00', 2, struct.pack("<H", new_start_addr))
        #self.send_single_write(0xff90, '\x01\x00', 2, struct.pack("<H", new_start_addr))
        #self.send_single_write(0x4fe8, '\x04\x07', 2, struct.pack("<H", new_start_addr))
        #self.send_single_write(0xf312, '\x04\x07', 2, struct.pack("<H", new_start_addr))

        self.send_single_write(0xff90, '\x01\x00', 4, struct.pack("<H", new_start_addr)+'\x04\x07')
        

    def full_download(self, download_directory):
        self.controlLogic_reset()

        self.write_data_file(download_directory)
        self.write_logic_file(download_directory)    

        self.write_zip_hash_file(download_directory)
        self.write_conf2_file(download_directory)
        self.write_data2_file(download_directory)
        self.write_conf_file(download_directory)

        #self.write_ff4e_file(download_directory)

        self.write_after_test()
        #self.status_check()

    def download_chksum_guess(self, download_directory):
        conf_file_name, conf_file_addr, conf_file_type, conf_file_size = self.get_conf_file_info()
        conf_file_data = read_local_file(download_directory, conf_file_name)
        i = 0x06bb

        while True:
            self.controlLogic_reset()

            self.write_data_file(download_directory)
            self.write_logic_file(download_directory)    

            self.write_zip_hash_file(download_directory)
            self.write_conf2_file(download_directory)
            self.write_data2_file(download_directory)

            #self.write_conf_file(download_directory)
            b = bytearray(conf_file_data)
            #print binascii.hexlify(b[-4:-2])
            b[-4:-2] = struct.pack("<H", i)
            self.write_file(conf_file_addr, conf_file_type, conf_file_size, b)

            self.write_after_test()
            self.start_controller()

            time.sleep(0.5)

            status = self.status_check()
            print status
            if status == True:
                break
            else:
                i+=1

    # This command delete existing control logic in a M221 PLC
    def controlLogic_reset(self):
        self.send_recv_msg(self.m221_sid+'\x80')

    def write_after_test(self):
        self.send_recv_msg(self.m221_sid+'\x81\x00\x00\x00\x00')
        self.send_recv_msg(self.m221_sid+'\x36\x01\x03')

    # check if control logic is successfully downloaded
    def status_check(self):
        recv = self.send_recv_msg('\x00\x04')
        if recv[11] == '\x0a':
            return True
        else:
            return False

    def write_conf_file(self, download_directory):
        conf_file_name, conf_file_addr, conf_file_type, conf_file_size = self.get_conf_file_info()
        conf_file_data = read_local_file(download_directory, conf_file_name)

        self.write_file(conf_file_addr, conf_file_type, conf_file_size, conf_file_data)

    # For chaning start address and size of code/data blocks
    def write_conf2_file(self, download_directory):
        conf_file_name, _, _, _ = self.get_conf_file_info()
        conf2_file_name, conf2_file_addr, conf2_file_type, conf2_file_size = self.get_conf2_file_info(download_directory, conf_file_name)
        conf2_file_data = read_local_file(download_directory, conf2_file_name)

        self.write_file(conf2_file_addr, conf2_file_type, conf2_file_size, conf2_file_data)

    def write_logic_file(self, download_directory):
        conf_file_name, _, _, _ = self.get_conf_file_info()
        conf2_file_name, _, _, _ = self.get_conf2_file_info(download_directory, conf_file_name)
        logic_file_name, logic_file_addr, logic_file_type, logic_file_size = self.get_logic_file_info(download_directory, conf2_file_name)
        logic_file_data = read_local_file(download_directory, logic_file_name)

        self.write_file(logic_file_addr, logic_file_type, logic_file_size, logic_file_data)

    def write_data_file(self, download_directory):
        conf_file_name, _, _, _ = self.get_conf_file_info()
        conf2_file_name, _, _, _ = self.get_conf2_file_info(download_directory, conf_file_name)
        data_file_name, data_file_addr, data_file_type, data_file_size = self.get_data_file_info(download_directory, conf2_file_name)
        data_file_data = read_local_file(download_directory, data_file_name)

        self.write_file(data_file_addr, data_file_type, data_file_size, data_file_data)

    def write_data2_file(self, download_directory):
        conf_file_name, _, _, _ = self.get_conf_file_info()
        conf2_file_name, _, _, _ = self.get_conf2_file_info(download_directory, conf_file_name)
        data2_file_name, data2_file_addr, data2_file_type, data2_file_size = self.get_data2_file_info(download_directory, conf2_file_name)
        data2_file_data = read_local_file(download_directory, data2_file_name)

        self.write_file(data2_file_addr, data2_file_type, data2_file_size, data2_file_data)

    def write_zip_hash_file(self, download_directory):
        conf_file_name, _, _, _ = self.get_conf_file_info()
        zip_file_name, zip_file_addr, zip_file_type, zip_file_size = self.get_zip_hash_file_info(download_directory, conf_file_name)
        zip_file_data = read_local_file(download_directory, zip_file_name)

        self.write_file(zip_file_addr, zip_file_type, zip_file_size, zip_file_data)

    def write_ff4e_file(self, download_directory):
        ff4e_file_addr, ff4e_file_type, ff4e_file_size = 0xff4e, '\x01\x00', 1
        ff4e_file_name = binascii.hexlify(struct.pack("<H", ff4e_file_addr) + ff4e_file_type)
        ff4e_file_data = read_local_file(download_directory, ff4e_file_name)

        self.write_file(ff4e_file_addr, ff4e_file_type, ff4e_file_size, ff4e_file_data)

    def start_controller(self):
        self.send_recv_msg(self.m221_sid+'\x40\xff\x00')
        
def read_local_file(dirName, fileName):
    filePath = str(os.path.abspath(dirName)) + "/" + fileName

    f = open(filePath, "r")
    data = f.read()
    f.close()
    return data
        
def save_file(dirName, fileName, data):
    filePath = str(os.path.abspath(dirName)) + "/" + fileName
    
    if not os.path.exists(os.path.dirname(filePath)):
        try:
            os.makedirs(os.path.dirname(filePath))
        except:
            print "exception occur in making dir: " + os.path.dirname(os.path.dirname(filePath))

    f = open(filePath, "w")
    print "Create file: " + filePath
    f.write(data)
    f.close()
        


def main():
    """
    m221_client = M221_client("137.30.122.151")
    m221_client.write_program()
    m221_client.close_connection()
    """
    parser = argparse.ArgumentParser(description="Simple M221 protocol client to download/upload control logic files")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-d", "--download-directory", help="download files in the directory to the PLC")
    group.add_argument("-u", "--upload-directory", help="upload files from the PLC")
    parser.add_argument("plc_ip", help="IP address of the target PLC")
    args = parser.parse_args()

    m221_client = M221_client(args.plc_ip)

    #m221_client.status_check()
    #sys.exit()

    #m221_client.start_controller()
    #sys.exit()
    
    if args.download_directory:
        print "Download files in {} to PLC".format(args.download_directory)
        #m221_client.download(args.download_directory)
        #m221_client.start_controller()
        #m221_client.download_chksum_guess(args.download_directory)
        #m221_client.full_download(args.download_directory)
        m221_client.write_logic_file_new_loc(args.download_directory, "00000407", 0x0000)
        m221_client.adjust_code_start(0x0000)
        m221_client.start_controller()

    elif args.upload_directory:
        print "Upload files from PLC to {}".format(args.upload_directory)
        m221_client.upload(args.upload_directory)

    m221_client.close_connection()

if __name__ == '__main__':
    main()
